# KisSplice

Main page: http://kissplice.prabi.fr/

# Downloading the code:
```
git clone --recursive https://forge.univ-lyon1.fr/p1508183/kissplice
```

# Compiling:
```
cd kissplice/ && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j 4
```

# Running on the sample test:
```
cd kissplice/build
bin/kissplice -r ../sample_example/reads1.fa -r ../sample_example/reads2.fa
```