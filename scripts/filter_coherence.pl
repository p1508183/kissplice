#!/usr/bin/perl -w


if(@ARGV !=7){
print "format:filter_coherence.pl coherence_condition1.txt coherence_condition2.txt sequences.fa checking.txt updatesequences.fa events_tab_43.txt events_tab_filt_43.txt \n";
print "Please specify the names of the input files and the output file\n"; 
exit 1;
}

$infile1=shift;  #idio me $infile=$ARGV[0]
#idio me $outfile= $ARGV[1]
$infile2=shift;
$infile3=shift;
$outfile=shift;
$outfile1=shift;
$infile4=shift;
$outfile2=shift;
open(IN, "$infile1") || die "Cannot open: $!\n";
open(IN2, "$infile2") || die "Cannot open: $!\n";
open(IN3, "$infile3") || die "Cannot open: $!\n";
open(OUT, ">$outfile") || die "Cannot open: $!\n";
open (OUT1,">$outfile1") || die "Cannot open: $!\n";
open(IN4, "$infile4") || die "Cannot open: $!\n";
open (OUT2,">$outfile2") || die "Cannot open: $!\n";

#>Cycle_10_upper_Length:88_coverage1:20_coverage2:17|UNCOHERENT_CGTCTACGAGGAGGTGCCCGCCTACGGACCATCCCGTGGCTACAGCAGCTATCCCCGCAGCCTGCGATCGGAGGGTAATGGAGGAAGT
$printme=0;
while($line=<IN>){
chomp($line);
if ($line=~m/^>Cycle_(\d+)_(\w+):(.*)\|UNCOHERENT(.*)/){
print $1;
$cyclenum=$1;
print "\t";
print $2;
$path=$2;
print "\t";
if ($path eq "upper_Length"){
$path=0;
}
else{
$path=1;
}
print $path;
print "\n";
$element=$cyclenum.$path;
push(@condition1,$element);
}
}
close(IN);

while($line=<IN2>){
chomp($line);
if ($line=~m/^>Cycle_(\d+)_(\w+):(.*)\|UNCOHERENT(.*)/){
print $1;
$cyclenum=$1;
print "\t";
print $2;
$path=$2;
print "\t";
if ($path eq "upper_Length"){
$path=0;
}
else{
$path=1;
}
print $path;
print "\n";
$element=$cyclenum.$path;
print $element;
print "\n";
push(@condition2,$element);
}
}
close(IN2);
$n1=0;
$n2=0;
@conditions = (@condition1, @condition2);
@conditions=sort{ $a <=> $b } (@conditions);
#while($condition1[$n1]){
#$n1++;
#while($condition2[$n2]){
#print OUT $condition1[$n1];
#print OUT "\n";
#if($condition1[$n1] == $condition2[$n2]){
#push(@cycles,$condition1[$n1]);
#}
#elsif($condition1[$n1] < $condition2[$n2]){
#break;
#}
#$n2++;
#}
#$n1++;
#}
foreach $i(@conditions){
print OUT $i;
print OUT "\n";
}
print OUT "\n\n\n\nDUPLICATES\n\n";
%h = ();
foreach $i (@conditions) {
    if (!exists($h{$i}))  {
        # First time we've seen this one
        $h{$i} = 0
    } elsif ($h{$i}) {
        # We've seen this one before and reported
        $h{$i}++
    } else {
        # Second time, so report the duplicate
        print OUT "\n $i\t";
$thesize=length($i);
$x=substr($i,0,$thesize-1);
push(@duplicates,$x);
print OUT "$x\n";
        $h{$i} = 1
    }
}
%h = (); 

$prev = -1;
               @duplicates = grep($_ ne $prev && ($prev = $_, 1), @duplicates);

foreach $i(@duplicates){
print OUT "$i\n";
}
$dups= join(" ",@duplicates);
print $dups;

while($line=<IN3>){
$found=0;
#>Cycle_0_upper_Length:87_coverage1:7_coverage2:7
#AGAACTGCACCACGAGGGGCTTGTCCTCGTTGGAGAAGCCATCGAACTTGCGGCTGGCCGCGTAGAATCGAGCATCCTGAGAGGTCT
chomp($line);
if ($line=~m/^>Cycle_(\d+)_(\w+):(.*)/){
$number=$1;
#print OUT1 "$number\n";
while($dups =~ m/(\d+)/g) {
	if ($number==$1){ 
$found=1;
}
}
if ($found==0){
print  OUT1 "$line";
print OUT1 "\n";
$line=<IN3>;
print  OUT1 "$line";
}
}
}

$counter=0;
while($line=<IN4>){
chomp($line);
$found=0;
if($counter==0){
print OUT2 $line;
print OUT2 "\n";
}
else{
#0	AGAACTGCACCACGAGGGGCTTGTCCTCGTTGGAGAAGCCATCGAACTTGCGGCTGGCCGCGTAGAATCGAGCATCCTGAGAGGTCT	AGAACTGCACCACGAGGGGCTTGTCCTCGTTGGAGAAGCCATCAAACTTGCGGCTGGCCGCGTAGAATCGAGCATCCTGAGAGGTCT	87	87	7	7	11	15	

if ($line=~m/^(\d+)\t(.*)/){
$number=$1;
#print OUT1 "$number\n";
while($dups =~ m/(\d+)/g) {
	if ($number==$1){ 
$found=1;
}
}
if ($found==0){
print  OUT2 "$line";
print OUT2 "\n";
}
}
}
$counter++;
}




close(IN3);
close(OUT);
close(OUT1);

