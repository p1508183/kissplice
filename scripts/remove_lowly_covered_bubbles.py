#!/usr/bin/env python
import sys

if len(sys.argv)!=4:
	print("[ERROR] usage:", sys.argv[0], "<file> <count_threshold> <min_nb_files>")
	sys.exit(1)

#read the args
filename = sys.argv[1]
countThreshold = int(sys.argv[2])
minNbFiles = int(sys.argv[3])

#compile the regex
import re
conditionPattern = re.compile("\C\d+\_(\d+)")

with open(filename) as file:
	while True:
		header1, seq1, header2, seq2 = [file.readline().rstrip() for _ in range(4)]

		if header1=="":
			break

		counts = [int(_) for _ in re.findall(conditionPattern, header1)]
		print(counts)
