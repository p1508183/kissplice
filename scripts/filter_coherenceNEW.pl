#!/usr/bin/perl -w



#>event_0_upper_path| A:183<206.22<239  B:104<154.08<182 |Cycle_0_upper_Length:101_coverage1:69_coverage2:63
#TTGATCCATAAATTGAGTCTGCAATAGTAAATGGAGCTTCAATATATTCATAAGCTTGAAGAATTGTAAAATAGATTCCTAGTAAAACTGTAAAAAATAAT

if(@ARGV !=3){
print "format:filter_coherenceNEW.pl coherent.txt events_tab_k.txt events_filtered.txt\n";
print "Please specify the names of the input files and the output file\n"; 
exit 1;
}

$infile1=shift;  #idio me $infile=$ARGV[0]
#idio me $outfile= $ARGV[1]
$infile2=shift;
$outfile=shift;

open(IN1, "$infile1") || die "Cannot open: $!\n";
open(IN2, "$infile2") || die "Cannot open: $!\n";
open(OUT, ">$outfile") || die "Cannot open: $!\n";


while($line=<IN1>){

#>event_0_upper_path| A:183<206.22<239  B:104<154.08<182 |Cycle_0_upper_Length:101_coverage1:69_coverage2:63
#TTGATCCATAAATTGAGTCTGCAATAGTAAATGGAGCTTCAATATATTCATAAGCTTGAAGAATTGTAAAATAGATTCCTAGTAAAACTGTAAAAAATAAT
chomp($line);
if ($line=~m/>event(.*)\|(.*)\|Cycle_(\d+)_(.*)/){

$number=$3;
#print OUT1 "$number\n";
#print $number;
push(@coherent,$number);
}

}
@hash{@coherent}=();


while($line=<IN2>){
#Cycle	Upperpath	LowerPath	Length_Upper	Length_Lower	Upper_Coverage_1	Upper_Coverage_2	Low_Coverage_1	Low_Coverage_2	Prediction
#0	TTGATCCATAAATTGAGTCTGCAATAGTAAATGGAGCTTCAATATATTCATAAGCTTGAAGAATTGTAAAATAGATTCCTAGTAAAACTGTAAAAAATAAT	TTGATCCATAAATTGAGTCTGCAATAGTAAATGGAGCTTCAATATATTCACAAGCTTGAAGAATTGTAAAATAGATTCCTAGTAAAACTGTAAAAAATAAT	101	101	69	63	3	2	
#2	GTTACTTTAGGGATAACAGCGTAATTTTTTTGGAGAGTTCATATCGATAAGAAAGATTGCGACCTCGATGTTGGATTAAGATATAATTTTGGGTGTAGCCG	GTTACTTTAGGGATAACAGCGTAATTTTTTTGGAGAGTTCATATCGATAACAAAGATTGCGACCTCGATGTTGGATTAAGATATAATTTTGGGTGTAGCCG	101	101	56	34	57	33	

chomp($line);

if ($line=~m/^Cycle(.*)/){
print OUT $line;
print OUT "\n";
}
if ($line=~m/^(\d+)\t(.*)/){
$number=$1;
print OUT "$line\n" if exists $hash{$number};
}

}




close(IN1);
close(IN2);
close(OUT);

