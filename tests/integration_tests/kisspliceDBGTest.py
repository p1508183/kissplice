#!/usr/bin/env python3
import re
from sys import argv
from os.path import dirname, abspath
import ProcessLauncher
TEST_INSTDIR=dirname(abspath(argv[0])) 
command_line = argv[1]+"/kissplice -k 25 -M 1000 -r "+TEST_INSTDIR+"/data/HBM75brain_100000.fasta -r "+TEST_INSTDIR+"/data/HBM75liver_100000.fasta --keep-counts -o "+ TEST_INSTDIR +"/results"
result = ProcessLauncher.run(command_line)

i=2
for fn in [TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.edges", TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes", TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance"]:
    fo=fn+"0"
    f=open(fn,"r")
    o=open(fo,"w")
    toWrite=[]
    for line in f:
        line=line.strip()
        lLine=line.split()
        toWrite.append(lLine[i])
    toWrite.sort()
    o.write("\n".join(toWrite))
    i-=1
    f.close()
    o.close()

command_line_diff_edges  = "diff " + TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.edges0 " + TEST_INSTDIR + "/data/graph_HBM75brain_100000_HBM75liver_100000_k25.edges0" 
result_diff_edges =  ProcessLauncher.run(command_line_diff_edges)

command_line_diff_nodes  = "diff " + TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes0 " + TEST_INSTDIR + "/data/graph_HBM75brain_100000_HBM75liver_100000_k25.nodes0" 
result_diff_nodes =  ProcessLauncher.run(command_line_diff_nodes)
 
command_line_diff_counts  = "diff " + TEST_INSTDIR + "/results/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance0 " + TEST_INSTDIR + "/data/graph_HBM75brain_100000_HBM75liver_100000_k25.abundance0" 
result_diff_counts =  ProcessLauncher.run(command_line_diff_counts)

# testing expected results
successful = True

if result_diff_edges.decode('utf8')  != "":
    successful = False

if result_diff_nodes.decode('utf8')  != "":
    successful = False
     
if result_diff_counts.decode('utf8') != "":
    successful = False

# summary
if successful:
    print("kisspliceDBGTest.py: test SUCCESSFUL")
else:
    print("kisspliceDBGTest.py: test FAILED")
    
command_line_rm = "rm -r "+TEST_INSTDIR +"/results"
ProcessLauncher.run(command_line_rm)