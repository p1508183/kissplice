/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA 
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
 
 
 
 
// ===========================================================================
//                               Include Libraries
// ===========================================================================



// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "NEdge.h"
#include "NGraph.h"
#include "Utils.h"



// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================




//############################################################################
//                                                                           #
//                                Class NEdge                                #
//                                                                           #
//############################################################################

// ===========================================================================
//                               Static attributes
// ===========================================================================

// ===========================================================================
//                                  Constructors
// ===========================================================================
NEdge::NEdge( const NEdge & model )
{
  node   = model.node;
  labels = model.labels;
}

NEdge::NEdge( int _node, string _labels )
{
  node    = _node;
  labels  = _labels;
}

NEdge::NEdge( int _node, char label1, char label2 )
{
  node = _node;
  
  char buf[3] = { label1, label2, '\0' };
  labels  = string( buf );
}


// ===========================================================================
//                                  Destructors
// ===========================================================================
NEdge::~NEdge( void )
{
}


// ===========================================================================
//                                   Operators
// ===========================================================================

// ===========================================================================
//                                 Public Methods
// ===========================================================================
void NEdge::add_label( string label )
{
  labels += label;
};

void NEdge::del_all_but_first( void )
{
  labels.erase( 2 );
};

void NEdge::revert_dir( int i )
{
  labels[i] = reverse_dir(labels[i]);
};

// ===========================================================================
//                                Protected Methods
// ===========================================================================

// ===========================================================================
//                              Non inline accessors
// ===========================================================================
