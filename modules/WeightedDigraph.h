#ifndef WEIGHTED_DIGRAPH_H
#define WEIGHTED_DIGRAPH_H

#include <vector>
using namespace std;

class WeightedEdge{
 public:
  int node;
  int cost; 
  bool removed;
  WeightedEdge(int n, int nCost) : node(n), cost(nCost), removed(false) { }
};

class WeightedDigraph{
 public:
  vector<vector<WeightedEdge> > adj_list;
  vector<bool> removed;
  
  vector<int> node_cost;
  WeightedDigraph(int nbNodes) : adj_list(nbNodes), removed(nbNodes, false), node_cost(nbNodes) { }
  
  void setArcStatus(bool status, int u, int v)
  {
    for (int i = 0; i < (int)adj_list[u].size(); i++)
      if (adj_list[u][i].node == v)
	adj_list[u][i].removed = status;
  }  

  int adjListSz(int node)
  {
    int sz = 0;
    for (int i = 0; i < (int)adj_list[node].size(); i++)
      sz += (!adj_list[node][i].removed && !removed[adj_list[node][i].node]);
    
    return sz;
  }
  int outDegree(int node)
  {
    return (int)adj_list[node].size();
  }  
};

#endif
