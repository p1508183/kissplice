# ============================================================================
# Require minimal version of cmake
# ============================================================================
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)


# ============================================================================
# Set project name and languages
# ============================================================================
PROJECT(kissplice CXX C)


# ============================================================================
# Get GNU standard installation directories (GNUInstallDirs module)
# ============================================================================
INCLUDE(GNUInstallDirs)


# ============================================================================
# Set compilation flags
# ============================================================================
IF(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE Distribution)
ENDIF(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE)


SET(CMAKE_CXX_FLAGS_DEBUG "-g -Wall")	      
SET(CMAKE_C_FLAGS_DEBUG "-g -Wall")
SET(CMAKE_CXX_FLAGS_RELEASE "-O3 -Wall")
SET(CMAKE_C_FLAGS_RELEASE "-O3 -Wall")
SET(CMAKE_CXX_FLAGS_DISTRIBUTION "-O3 -w")
SET(CMAKE_C_FLAGS_DISTRIBUTION "-O3 -w")
SET(CMAKE_CXX_FLAGS_PROFILING "-g -pg") 
SET(CMAKE_C_FLAGS_PROFILING "-g -pg")



# ============================================================================
# Manage the KMERS_OVER_32 option
# The default value for KMERS_OVER_32 is
#   * ON on 64 bits systems
#   * OFF on 32 bits systems
# If a user tries to set KMERS_OVER_32 to ON on a 32 bits system, it will be
# forced back to OFF in the CACHE and a FATAL_ERROR message will be issued.
# ============================================================================
SET(KMERS_OVER_32_HELP_STRING "Increase the maximum k-mer size on 64-bits systems")

IF (${CMAKE_SIZEOF_VOID_P} LESS 8 )
  set(KMERS_OVER_32 OFF CACHE BOOL KMERS_OVER_32_HELP_STRING)
  IF (KMERS_OVER_32)
    set(KMERS_OVER_32 OFF CACHE BOOL KMERS_OVER_32_HELP_STRING FORCE)
    MESSAGE(FATAL_ERROR
            "The KMERS_OVER_32 option cannot be used on 32 bits systems")
  ENDIF ()
ELSE ()
  set(KMERS_OVER_32 ON CACHE BOOL KMERS_OVER_32_HELP_STRING)
ENDIF ()


# ============================================================================
# Tell cmake where to put binary files.
# By GNU standards "executable programs that users can run" should go in
# bindir a.k.a ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}
# and "executable programs to be run by other programs rather than by users"
# in libexecdir a.k.a ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBEXECDIR}
#
# The main script of kissplice (kissplice.py) must be configured to find the
# secondary binaries. These can be found either in libexecdir (after install)
# or in the build-tree, we hence need 2 versions of kissplice: one that uses
# those secondary binaries in the build-tree and one that is installable.
# The latter will be placed in an ad-hoc directory called "install" in the
# build-tree
#
# Since all the files to be *compiled* belong to libexecdir, we can set
# ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} once and for all her
# ============================================================================
# Set main binary dir
SET(MAIN_BIN_DIR ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
# Set the location of other executables
SET(SEC_BIN_DIR ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBEXECDIR}/kissplice)

# Tell cmake where to put compiled files
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${SEC_BIN_DIR})

# Make a configured copy of kissplice main (python) file
# KS_SEC_EXEC_PATH must be a relative path to support local installations using
# make install DESTDIR=/some/path
set(KS_SEC_EXEC_PATH ${CMAKE_INSTALL_LIBEXECDIR}/kissplice)
configure_file(${PROJECT_SOURCE_DIR}/kissplice.py ${MAIN_BIN_DIR}/kissplice @ONLY)

# Tell cmake where to install the main script (kissplice)
install(PROGRAMS ${MAIN_BIN_DIR}/kissplice
        DESTINATION ${CMAKE_INSTALL_FULL_BINDIR})


# ============================================================================
# zlib required for kissreads
# ============================================================================
FIND_PACKAGE(ZLIB REQUIRED)


# ============================================================================
# Tell cmake about subdirectories to look into
# ============================================================================
ADD_SUBDIRECTORY(bcalm)
ADD_SUBDIRECTORY(thirdparty)
ADD_SUBDIRECTORY(modules)
ADD_SUBDIRECTORY(man)
ADD_SUBDIRECTORY(doc)


# ============================================================================
# Add testing capabilities
# ============================================================================
ENABLE_TESTING()
ADD_SUBDIRECTORY(tests)


# ============================================================================
# Adds the 'dist' target (that will use cpack)
# ============================================================================
add_custom_target(dist COMMAND ${CMAKE_BUILD_TOOL} package_source)


# ============================================================================
# Add custom uninstall target
# ============================================================================
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/utils/cmake/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)




# ============================================================================
# Configure cpack
# ============================================================================
SET(CPACK_PACKAGE_NAME "kissplice")
SET(CPACK_PACKAGE_VENDOR "Kissplice Development Team")
SET(CPACK_PACKAGE_VERSION_MAJOR "2")
SET(CPACK_PACKAGE_VERSION_MINOR "5")
SET(CPACK_PACKAGE_VERSION_PATCH "3")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Splicing events caller")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
SET(CPACK_RESOURCE_FILE_AUTHORS "${CMAKE_SOURCE_DIR}/AUTHORS")
SET(CPACK_RESOURCE_FILE_INSTALL "${CMAKE_SOURCE_DIR}/INSTALL")
SET(CPACK_SOURCE_GENERATOR "TGZ")
SET(CPACK_SOURCE_IGNORE_FILES
 "README.CMake"
 "README.Licence"
 "README.Release"
 "README.NamingConventions"
 "README.Kissreads"
 "README.md"
 "CMakeFiles"
 "Makefile"
 "_CPack_Packages"
 "CMakeCache.txt"
 ".*\\\\.svn"
 ".*\\\\.idea"
 ".*\\\\.DS_Store"
 ".*\\\\.gz"
 ".*\\\\~"
 ".*\\\\.o"
 "bcc/"
 "datasets"
 "ismb"
 "Recomb"
 "RR"
 "script_tarjan_aphid.sh"
 "splice_sites_results"
 "splicing_events"
 "TODO.*"
 "Validation"
 "validation_script.sh"
 ".*project"
 "/\\\\.git*"
 "^results$"
 "^build$"
 "incremental-bicon.ps"
 "^scripts$"
 "skeletons"
 "doc/dev"
 "alt_splicing_sample_ex.png"
 "quantifModel-big.png"
 "resType0.png"
 "resType1.png"
 "quantifModel.png"
 "ucsc.png"
 "user_guide.aux"
 "user_guide.bib"
 "user_guide.log"
 "user_guide.toc"
 "user_guide.tex"
 "jobim13"
 ${CPACK_SOURCE_IGNORE_FILES}
) 
SET(CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
include(CPack)
